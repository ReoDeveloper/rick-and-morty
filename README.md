# Rick and Morty

Create a native Android app that displays a list of characters from Rick And Morty.

## Requirements
* Use https://rickandmortyapi.com/api/character/?page=1 to retrieve te list of characters and their image. Ignore other pages and pagination.
* Design the application to support orientation changes.
* When clicking on a character a new screen should be shown with the image of the character, the gender and the created time.
* Create one UI Test for the click action on the list.

## Considerations
* Use material design.
* Minimum API Level 21.
* Use as many libraries as you see fit.
* Limit or extend the reqiurements as you see fit.