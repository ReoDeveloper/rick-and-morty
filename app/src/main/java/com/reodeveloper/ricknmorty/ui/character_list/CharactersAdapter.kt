package com.reodeveloper.ricknmorty.ui.character_list

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.reodeveloper.ricknmorty.R
import com.reodeveloper.ricknmorty.domain.model.Character
import com.reodeveloper.ricknmorty.ui.inflate
import com.reodeveloper.ricknmorty.ui.loadUrl
import kotlinx.android.synthetic.main.character_list_item.view.*

class CharactersAdapter(val characters: List<Character>, private val listener: (Character) -> Unit) :
        RecyclerView.Adapter<CharactersAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.character_list_item))
    }

    override fun getItemCount() = characters.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(characters[position], listener)
    }

    class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
        fun bind(item:Character, listener:(Character)->Unit) = with(itemView){
            txt_name.text = item.name
            txt_species.text = item.species
            txt_status.text = item.status
            img_avatar.loadUrl(item.imageUrl)
            setOnClickListener { listener(item) }
        }
    }
}