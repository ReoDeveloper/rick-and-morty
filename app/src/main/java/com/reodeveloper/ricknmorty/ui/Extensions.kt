package com.reodeveloper.ricknmorty.ui

import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.Snackbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.squareup.picasso.Picasso

fun ViewGroup.inflate(layoutRes: Int, attachToRoot : Boolean = false) : View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}

fun ImageView.loadUrl(url: String) {
    Picasso.get().load(url).into(this)
}

fun CoordinatorLayout.showMessage(message:String, duration:Int = Snackbar.LENGTH_INDEFINITE){
    Snackbar.make(this, message, duration).show()
}