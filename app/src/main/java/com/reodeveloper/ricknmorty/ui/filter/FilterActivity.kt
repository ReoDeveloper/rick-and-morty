package com.reodeveloper.ricknmorty.ui.filter


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.reodeveloper.ricknmorty.R
import com.reodeveloper.ricknmorty.ui.character_list.CharacterListActivity
import kotlinx.android.synthetic.main.activity_filter.*
import java.util.Arrays.asList

class FilterActivity : AppCompatActivity() {

    var currentFilter = Filter("", "", "")

    val possibleGender = asList(R.id.chip_gender_genderless, R.id.chip_gender_female, R.id.chip_gender_male, R.id.chip_gender_unknown)
    val possibleStatus = asList(R.id.chip_status_alive, R.id.chip_status_dead, R.id.chip_status_unknown)

    companion object {
        fun startActivity(context: AppCompatActivity, requestCode: Int) {
            Intent(context, FilterActivity::class.java).also { context.startActivityForResult(it, requestCode) }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filter)
        chip_group_gender.setOnCheckedChangeListener { chipGroup, resId ->
            currentFilter.gender = when (possibleGender.indexOf(resId)) {
                0 -> getString(R.string.txt_status_genderless)
                1 -> getString(R.string.txt_status_female)
                2 -> getString(R.string.txt_status_male)
                else -> getString(R.string.txt_status_unknown)
            }
        }
        chip_group_status.setOnCheckedChangeListener { chipGroup, resId ->
            currentFilter.status = when (possibleStatus.indexOf(resId)) {
                0 -> getString(R.string.txt_status_alive)
                1 -> getString(R.string.txt_status_dead)
                else -> getString(R.string.txt_status_unknown)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.filter_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.perform_filter -> {
                sendDataBack()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun sendDataBack() {
        currentFilter.species = edit_species.text.toString()
        intent = Intent().run {
            putExtra(CharacterListActivity.FILTER_GENDER, currentFilter.gender)
            putExtra(CharacterListActivity.FILTER_STATUS, currentFilter.status)
            putExtra(CharacterListActivity.FILTER_SPECIES, currentFilter.species)
        }
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    data class Filter(var gender: String, var status: String, var species: String)
}