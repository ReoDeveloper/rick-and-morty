package com.reodeveloper.ricknmorty.ui.character_list

import android.content.Context
import com.reodeveloper.ricknmorty.common.usecase.UseCase
import com.reodeveloper.ricknmorty.domain.model.Character

interface CharacterListContract {
    interface View {
        fun showError(message: String)
        fun displayItems(items: List<Character>)
        fun showLoading()
        fun hideLoading()
        fun goToDetail(id: Int)
        fun displayInfoInColumns(num: Int)
        fun isViewInPortrait(): Boolean
    }

    interface Actions {
        fun init()
        fun onItemTap(item: Character)
        fun onTextSearched(usecase: UseCase<Character>)
        fun onFilterReceived(context: Context, gender: String, status:String, species: String)
    }
}