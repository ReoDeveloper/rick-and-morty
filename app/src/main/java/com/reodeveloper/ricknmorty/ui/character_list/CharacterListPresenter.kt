package com.reodeveloper.ricknmorty.ui.character_list

import android.content.Context
import com.reodeveloper.ricknmorty.common.UseCaseProvider
import com.reodeveloper.ricknmorty.common.usecase.Executor
import com.reodeveloper.ricknmorty.common.usecase.ResultList
import com.reodeveloper.ricknmorty.common.usecase.UseCase
import com.reodeveloper.ricknmorty.data.SpecificationByGender
import com.reodeveloper.ricknmorty.data.SpecificationBySpecies
import com.reodeveloper.ricknmorty.data.SpecificationByStatus
import com.reodeveloper.ricknmorty.data.SpecificationMultiple
import com.reodeveloper.ricknmorty.domain.model.Character
import com.reodeveloper.ricknmorty.domain.usecases.GetAllCharacters

class CharacterListPresenter(val view: CharacterListContract.View, private val usecase: GetAllCharacters) : CharacterListContract.Actions {

    private val executor: Executor = Executor.getInstance()
    private val callback = object : ResultList<Character> {
        override fun error(message: String) {
            view.showError(message)
            view.hideLoading()
        }

        override fun success(items: List<Character>) {
            view.displayInfoInColumns(if (view.isViewInPortrait()) 2 else 3)
            view.displayItems(items)
            view.hideLoading()
        }
    }

    override fun init() {
        view.showLoading()
        executor.execute(usecase, callback)
    }

    override fun onItemTap(item: Character) {
        view.goToDetail(item.id)
    }

    override fun onTextSearched(usecase: UseCase<Character>) {
        executor.execute(usecase, callback)
    }

    override fun onFilterReceived(context: Context, gender: String, status: String, species: String) {
        view.showLoading()

        // Filter by gender
        val byGender = SpecificationByGender(gender)
        // Filter by status
        val byStatus = SpecificationByStatus(status)
        // Filter by species
        val bySpecies = SpecificationBySpecies(species)

        val specification = SpecificationMultiple().apply {
            addSpecification(byGender)
            addSpecification(byStatus)
            addSpecification(bySpecies)
        }

        executor.execute(UseCaseProvider.provideGetCharactersBySpecification(context, specification), callback)
    }
}