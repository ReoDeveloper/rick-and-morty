package com.reodeveloper.ricknmorty.ui.character_item

import com.reodeveloper.ricknmorty.R
import com.reodeveloper.ricknmorty.common.usecase.Executor
import com.reodeveloper.ricknmorty.common.usecase.ResultItem
import com.reodeveloper.ricknmorty.domain.model.Character
import com.reodeveloper.ricknmorty.domain.usecases.GetCharacterById

class CharacterPresenter(val view: CharacterContract.View, private val usecase: GetCharacterById) : CharacterContract.Actions {

    private val executor: Executor = Executor.getInstance()
    private val possibleGender = listOf("Male", "Female", "genderless", "unknown")
    private val possibleStatus = listOf("unknown", "Dead", "Alive")

    override fun init(idCharacter: Int) {
        if (idCharacter < 0) {
            view.showError("Invalid idCharacter [$idCharacter] received")
            return
        }
        view.showLoading()
        executor.execute(usecase, object : ResultItem<Character> {
            override fun error(message: String) {
                view.showError(message)
                view.hideLoading()
            }

            override fun success(item: Character) {
                view.displayName(item.name)
                view.displaySpecies(item.species)

                if (item.type.isBlank()){
                    view.hideType()
                }else{
                    view.displayType(item.type)
                }

                view.displayAvatar(item.imageUrl)
                view.displayCreationTime(item.created)
                displayGender(item.gender)
                displayStatus(item.status)
                view.displayOrigin(item.origin)
                view.displayLocation(item.location)
                view.hideLoading()
            }
        })
    }

    fun displayGender(value: String) {
        when (possibleGender.indexOf(value)) {
            0 -> view.displayGender(R.drawable.ic_male, R.string.txt_status_male)
            1 -> view.displayGender(R.drawable.ic_female, R.string.txt_status_female)
            2 -> view.displayGender(R.drawable.ic_genderless, R.string.txt_status_genderless)
            3 -> view.displayGender(R.drawable.ic_unknown, R.string.txt_status_unknown)
        }
    }

    fun displayStatus(value: String) {
        when (possibleStatus.indexOf(value)) {
            0 -> view.displayStatus(R.drawable.ic_unknown, R.string.txt_status_unknown)
            1 -> view.displayStatus(R.drawable.ic_dead, R.string.txt_status_dead)
            2 -> view.displayStatus(R.drawable.ic_alive, R.string.txt_status_alive)
        }
    }
}