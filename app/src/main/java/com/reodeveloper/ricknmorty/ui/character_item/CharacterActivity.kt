package com.reodeveloper.ricknmorty.ui.character_item

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.reodeveloper.ricknmorty.R
import com.reodeveloper.ricknmorty.common.UseCaseProvider
import com.reodeveloper.ricknmorty.ui.loadUrl
import com.reodeveloper.ricknmorty.ui.showMessage
import kotlinx.android.synthetic.main.activity_character_item.*
import kotlinx.android.synthetic.main.character_details.*

class CharacterActivity : AppCompatActivity(), CharacterContract.View {

    companion object {
        val ID_CHARACTER = "idCharacter"

        fun startActivity(context: Context, idCharacter: Int) {
            Intent(context, CharacterActivity::class.java).apply {
                putExtra(ID_CHARACTER, idCharacter)
            }.also { context.startActivity(it) }
        }
    }

    lateinit var presenter: CharacterPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_character_item)
        setupToolbar()
        val idCharacter = intent.getIntExtra(ID_CHARACTER, -1)

        presenter = CharacterPresenter(this, UseCaseProvider.provideGetCharacterById(this, idCharacter))
        presenter.init(idCharacter)
    }

    fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.run {
            setHomeButtonEnabled(true)
            setDisplayHomeAsUpEnabled(true)
        }
    }

    override fun displayName(value: String) {
        collapsing_toolbar_layout.title = value
    }

    override fun displayAvatar(value: String) {
        img_avatar.loadUrl(value)
    }

    override fun displayGender(image:Int, text:Int) {
        img_character_gender.setImageResource(image)
        txt_character_gender.text = getText(text)
    }

    override fun displayCreationTime(value: String) {
        txt_character_creation_time.text = value
    }

    override fun displayStatus(image:Int, text:Int) {
        img_character_status.setImageResource(image)
        txt_character_status.text = getText(text)
    }

    override fun displaySpecies(value: String) {
        txt_character_species.text = value
    }

    override fun displayOrigin(value: String) {
        txt_character_origin.text = value
    }

    override fun displayLocation(value: String) {
        txt_character_location.text = value
    }

    override fun showError(message: String) {
        coordinator_layout.showMessage(message)
    }

    override fun showLoading() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progressBar.visibility = View.GONE
    }

    override fun displayType(value: String) {
        lay_character_type.visibility = View.VISIBLE
        txt_character_type.text = value
    }

    override fun hideType() {
        lay_character_type.visibility = View.GONE
    }
}