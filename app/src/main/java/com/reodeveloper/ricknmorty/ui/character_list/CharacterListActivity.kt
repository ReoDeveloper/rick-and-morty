package com.reodeveloper.ricknmorty.ui.character_list

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.SearchView
import android.view.*
import android.widget.Toast
import com.reodeveloper.ricknmorty.R
import com.reodeveloper.ricknmorty.common.UseCaseProvider
import com.reodeveloper.ricknmorty.data.SpecificationByName
import com.reodeveloper.ricknmorty.domain.model.Character
import com.reodeveloper.ricknmorty.ui.character_item.CharacterActivity
import com.reodeveloper.ricknmorty.ui.filter.FilterActivity
import kotlinx.android.synthetic.main.activity_character_list.*

class CharacterListActivity : AppCompatActivity(), CharacterListContract.View {

    companion object {
        val FILTER_ACTIVITY_REQUEST = 1984
        val FILTER_GENDER = "gender"
        val FILTER_STATUS = "status"
        val FILTER_SPECIES = "species"
    }

    lateinit var presenter: CharacterListContract.Actions

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_character_list)

        presenter = CharacterListPresenter(this, UseCaseProvider.provideGetAllCharactersUseCase(this))
        presenter.init()
        handleIntent(intent)
    }

    override fun onNewIntent(intent: Intent) {
        handleIntent(intent)
    }

    override fun displayInfoInColumns(num: Int) {
        recyclerView.layoutManager = GridLayoutManager(this, num)
    }

    override fun showError(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun displayItems(items: List<Character>) {
        recyclerView.adapter = CharactersAdapter(items) { presenter.onItemTap(it) }
    }

    override fun showLoading() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progressBar.visibility = View.GONE
    }

    override fun goToDetail(id: Int) {
        CharacterActivity.startActivity(this, id)
    }


    override fun isViewInPortrait(): Boolean {
        val rotation = (baseContext.getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay.rotation
        return when (rotation) {
            Surface.ROTATION_0 -> true
            Surface.ROTATION_180 -> true
            else -> false
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)

        // Associate searchable configuration with the SearchView
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        (menu.findItem(R.id.search).actionView as SearchView).apply {
            setSearchableInfo(searchManager.getSearchableInfo(componentName))
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.filter -> FilterActivity.startActivity(this, FILTER_ACTIVITY_REQUEST)
        }
        return super.onOptionsItemSelected(item)
    }

    private fun handleIntent(intent: Intent) {
        if (Intent.ACTION_SEARCH == intent.action) {
            val query = intent.getStringExtra(SearchManager.QUERY)
            val specification = SpecificationByName(query)
            val searchUseCase = UseCaseProvider.provideGetCharactersBySpecification(this, specification)
            presenter.onTextSearched(searchUseCase)
        }
    }

    override  fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            FILTER_ACTIVITY_REQUEST -> {
                data?.run {
                    presenter.onFilterReceived(baseContext, getStringExtra(FILTER_GENDER),
                            getStringExtra(FILTER_STATUS), getStringExtra(FILTER_SPECIES))
                }
            }
        }
    }
}
