package com.reodeveloper.ricknmorty.ui.character_item

interface CharacterContract {
    interface View {
        fun displayName(value:String)
        fun displayAvatar(value:String)
        fun displayGender(image:Int, text:Int)
        fun displayCreationTime(value:String)
        fun displayStatus(image:Int, text:Int)
        fun displaySpecies(value:String)
        fun displayType(value:String)
        fun displayOrigin(value:String)
        fun displayLocation(value:String)
        fun hideType()
        fun showError(message:String)
        fun showLoading()
        fun hideLoading()
    }
    interface Actions {
        fun init(idCharacter:Int)
    }
}