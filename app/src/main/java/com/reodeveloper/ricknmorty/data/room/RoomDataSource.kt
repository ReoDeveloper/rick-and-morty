package com.reodeveloper.ricknmorty.data.room

import android.content.Context
import com.reodeveloper.ricknmorty.common.DataSource
import com.reodeveloper.ricknmorty.data.*
import com.reodeveloper.ricknmorty.data.retrofit.SpecificationByPage
import com.reodeveloper.ricknmorty.domain.model.Character

class RoomDataSource(val context: Context, val mapper: TwoWayMapper<DbCharacter, Character>) : DataSource<Character> {

    var database: RickNMortyDatabase = RickNMortyDatabase.getInstance(context)

    override fun store(item: Character) {
        database.charactersDao().insert(mapper.reverseMap(item))
    }

    override fun store(items: List<Character>) {
        items.map { store(it) }
    }

    override fun getAll(): List<Character> {
        return mapper.map(database.charactersDao().getAll())
    }

    override fun queryList(specification: Specification): List<Character> {
        return when (specification) {
            is SpecificationByName -> mapper.map(database.charactersDao().queryItemsByName(specification.name))
            is SpecificationByStatus -> mapper.map(database.charactersDao().queryItemsByStatus(specification.status))
            is SpecificationBySpecies -> mapper.map(database.charactersDao().queryItemsBySpecies(specification.species))
            is SpecificationByGender -> mapper.map(database.charactersDao().queryItemsByGender(specification.gender))
            is SpecificationMultiple -> mapper.map(database.charactersDao().queryItemsByMultiple(specification.gender, specification.status, specification.species))
            else -> emptyList()
        }
    }

    override fun queryItem(specification: Specification): Character {
        return when (specification) {
            is SpecificationByPage -> throw IllegalArgumentException("Cannot use SpecificationByPage on RoomDataSource")
            is SpecificationById -> mapper.map(database.charactersDao().queryItemById(specification.id))
            else -> getAll()[0]
        }
    }
}