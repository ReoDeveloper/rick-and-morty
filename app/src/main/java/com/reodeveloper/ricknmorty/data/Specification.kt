package com.reodeveloper.ricknmorty.data

// This provides a common ancestor
interface Specification {
    // To be defined on the children
}

class SpecificationMultiple : Specification {
    var name: String = ""
    var status: String = ""
    var species: String = ""
    var gender: String = ""

    fun addSpecification(specification: Specification) {
        when (specification) {
            is SpecificationByName -> name = specification.name
            is SpecificationByStatus -> status = specification.status
            is SpecificationBySpecies -> species = specification.species
            is SpecificationByGender -> gender = specification.gender
        }
    }
}

data class SpecificationById(val id: Int) : Specification
data class SpecificationByName(val name: String) : Specification
data class SpecificationByStatus(val status: String) : Specification
data class SpecificationBySpecies(val species: String) : Specification
data class SpecificationByGender(val gender: String) : Specification