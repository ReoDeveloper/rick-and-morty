package com.reodeveloper.ricknmorty.data.retrofit

import com.reodeveloper.ricknmorty.data.Specification

// This Specification is only relevant to Retrofit repository
data class SpecificationByPage(val page: Int) : Specification