package com.reodeveloper.ricknmorty.data.retrofit

import com.reodeveloper.ricknmorty.data.Mapper
import com.reodeveloper.ricknmorty.domain.model.Character
import java.text.SimpleDateFormat
import java.util.*

class RfCharacterMapper : Mapper<ApiCharacter, Character>() {

    override fun map(item: ApiCharacter): Character {
        item.run {
            return Character(
                    id,
                    name,
                    gender,
                    formatDate(created),
                    image,
                    status,
                    species,
                    type,
                    origin.name,
                    location.name
            )
        }
    }

    private fun formatDate(input:String): String {
        return SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX", Locale.ENGLISH).parse(input).let {
            SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss", Locale.ENGLISH).format(it)
        }
    }
}