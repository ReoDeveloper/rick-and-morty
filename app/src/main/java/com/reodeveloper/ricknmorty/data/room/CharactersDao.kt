package com.reodeveloper.ricknmorty.data.room

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query

@Dao interface CharactersDao {
    @Query("SELECT * from charactersTable")
    fun getAll(): List<DbCharacter>

    @Query("SELECT * from charactersTable WHERE name LIKE '%' || :name || '%'")
    fun queryItemsByName(name:String): List<DbCharacter>

    @Query("SELECT * from charactersTable WHERE gender LIKE :gender AND status LIKE :status AND species LIKE '%' || :species || '%'")
    fun queryItemsByMultiple(gender:String, status:String, species:String): List<DbCharacter>

    @Query("SELECT * from charactersTable WHERE status LIKE :status")
    fun queryItemsByStatus(status:String): List<DbCharacter>

    @Query("SELECT * from charactersTable WHERE species LIKE '%' || :species || '%'")
    fun queryItemsBySpecies(species:String): List<DbCharacter>

    @Query("SELECT * from charactersTable WHERE gender LIKE :gender")
    fun queryItemsByGender(gender:String): List<DbCharacter>

    @Query("SELECT * from charactersTable WHERE id = :idCharacter")
    fun queryItemById(idCharacter:Int):DbCharacter

    @Insert(onConflict = REPLACE)
    fun insert(item: DbCharacter)

}