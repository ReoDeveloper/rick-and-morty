package com.reodeveloper.ricknmorty.data.room

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "charactersTable")
data class DbCharacter (@PrimaryKey val id:Int,
                        var name:String, var status:String, var species:String, var type:String,
                        var gender:String, var origin: String, var location: String,
                        var image:String, var created:String)