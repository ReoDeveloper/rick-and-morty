package com.reodeveloper.ricknmorty.data

import com.reodeveloper.ricknmorty.common.DataSource
import com.reodeveloper.ricknmorty.common.Repository
import com.reodeveloper.ricknmorty.domain.model.Character

class CharactersRepository(dataSource: DataSource<Character>) : Repository<Character>(dataSource) {
    var cache: DataSource<Character>? = null

    override fun store(item: Character) {
        cache?.store(item)
    }

    override fun store(items: List<Character>) {
        cache?.store(items)
    }

    override fun getAll(): List<Character> {
        // TODO: This code is ugly, upgrade it later
        val result = cache?.getAll()
        if (result?.isNotEmpty()!!) {
            return result
        }
        // This one is fine
        return super.getAll().also { cache?.store(it) }
    }

    override fun queryItem(specification: Specification): Character {
        cache?.run {
            queryItem(specification).also { return it }
        }
        return super.queryItem(specification)
    }

    override fun queryList(specification: Specification): List<Character> {
        val apiResult = super.queryList(specification)
        cache?.run {
            val dbResult = queryList(specification)
            if (apiResult.size > dbResult.size) {
                store(apiResult)
                return apiResult
            }
            return dbResult
        }
        return apiResult
    }

}