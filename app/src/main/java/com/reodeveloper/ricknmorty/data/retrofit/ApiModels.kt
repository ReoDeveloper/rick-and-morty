package com.reodeveloper.ricknmorty.data.retrofit

data class ApiOrigin(val name:String, val url:String)
data class ApiLocation(val name:String, val url:String)
data class ApiCharacter(val id:Int, val name:String, val status:String, val species:String,
                        val type:String, val gender:String, val origin: ApiOrigin,
                        val location: ApiLocation, val image:String, val episode:List<String>,
                        val url:String, val created:String)
data class ApiInfoResponse(val count:Int, val pages:Int, val next:String, val prev:String)
data class ApiCharactersResponse (val info: ApiInfoResponse, val results:List<ApiCharacter>)