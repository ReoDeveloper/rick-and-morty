package com.reodeveloper.ricknmorty.data.retrofit

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RickAndMortyApiService {
    @GET("character")
    fun getCharactersPage(@Query("page") page: Int): Call<ApiCharactersResponse>

    @GET("character/{id}")
    fun getCharacterById(@Path("id") id: Int): Call<ApiCharacter>

    @GET("character")
    fun getCharactersByName(@Query("name") value: String): Call<ApiCharactersResponse>

    @GET("character")
    fun getCharactersByStatus(@Query("status") value: String): Call<ApiCharactersResponse>

    @GET("character")
    fun getCharactersBySpecies(@Query("species") value: String): Call<ApiCharactersResponse>

    @GET("character")
    fun getCharactersByGender(@Query("gender") value: String): Call<ApiCharactersResponse>

    @GET("character")
    fun getCharactersByMultiple(@Query("status") status: String,
                                @Query("gender") gender: String,
                                @Query("species") species: String): Call<ApiCharactersResponse>
}