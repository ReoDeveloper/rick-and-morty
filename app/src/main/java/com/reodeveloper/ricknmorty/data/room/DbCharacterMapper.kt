package com.reodeveloper.ricknmorty.data.room

import com.reodeveloper.ricknmorty.data.TwoWayMapper
import com.reodeveloper.ricknmorty.domain.model.Character

class DbCharacterMapper : TwoWayMapper<DbCharacter, Character>() {
    override fun map(item: DbCharacter): Character {
        item.run {
            return Character(
                    id,
                    name,
                    gender,
                    created,
                    image,
                    status,
                    species,
                    type,
                    origin,
                    location
            )
        }
    }


    override fun reverseMap(item: Character): DbCharacter {
        item.run {
            return DbCharacter(
                    id,
                    name,
                    status,
                    species,
                    type,
                    gender,
                    origin,
                    location,
                    imageUrl,
                    created
            )
        }
    }
}