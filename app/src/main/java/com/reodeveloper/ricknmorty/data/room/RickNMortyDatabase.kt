package com.reodeveloper.ricknmorty.data.room

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context

@Database(entities = arrayOf(DbCharacter::class), version = 1)
abstract class RickNMortyDatabase : RoomDatabase() {

    abstract fun charactersDao(): CharactersDao

    companion object {
        private var INSTANCE: RickNMortyDatabase? = null
        private val DB_NAME = "ricknmorty"

        fun getInstance(context: Context): RickNMortyDatabase {
            if (INSTANCE == null) {
                synchronized(RickNMortyDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                                                    RickNMortyDatabase::class.java,
                                                    DB_NAME)
                                    .build()
                }
            }
            return INSTANCE!!
        }

        fun destroyInstance(){
            INSTANCE = null
        }
    }
}