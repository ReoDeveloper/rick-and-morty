package com.reodeveloper.ricknmorty.data.retrofit

import com.reodeveloper.ricknmorty.common.DataSource
import com.reodeveloper.ricknmorty.data.*
import com.reodeveloper.ricknmorty.domain.model.Character
import retrofit2.Call

class RetrofitDataSource(val mapper: Mapper<ApiCharacter, Character>, url:String = BASE_URL) : DataSource<Character> {

    companion object {
        val BASE_URL = "https://rickandmortyapi.com/api/"
    }

    val emptyCharacter = Character(-1, "", "", "", "", "", "", "", "", "")
    val apiService: RickAndMortyApiService =
            RetrofitClient.getClient(url)!!.create(RickAndMortyApiService::class.java)

    override fun store(item: Character) {
        // This is method is not supported for the api
        throw UnsupportedOperationException("This Datasource has no storing capabilities")
    }

    override fun store(items: List<Character>) {
        // This is method is not supported for the api
        throw UnsupportedOperationException("This Datasource has no storing capabilities")
    }

    override fun getAll(): List<Character> {
        val call = apiService.getCharactersPage(1)
        val callResponse = call.execute()
        if(callResponse?.isSuccessful!!){
            val apiCharactersResponse = callResponse.body()
            apiCharactersResponse?.let {
                return mapper.map(it.results)
            }
        }
        return emptyList()
    }

    override fun queryList(specification: Specification): List<Character> {
        return when(specification){
            is SpecificationByName -> queryListByName(specification.name)
            is SpecificationByStatus -> queryListByStatus(specification.status)
            is SpecificationBySpecies -> queryListBySpecies(specification.species)
            is SpecificationByGender -> queryListByGender(specification.gender)
            is SpecificationMultiple -> queryListByMultiple(specification)
            else -> emptyList()
        }
    }

    private fun queryList(call: Call<ApiCharactersResponse>): List<Character> {
        val callResponse = call.execute()
        if(callResponse?.isSuccessful!!){
            val apiCharactersResponse = callResponse.body()
            apiCharactersResponse?.let{ return mapper.map(it.results)}
        }
        return emptyList()
    }

    private fun queryItem(call: Call<ApiCharacter>): Character {
        val callResponse = call.execute()
        if(callResponse?.isSuccessful!!){
            val apiCharactersResponse = callResponse.body()
            apiCharactersResponse?.let{ return mapper.map(it)}
        }
        return emptyCharacter
    }

    private fun queryListByMultiple(specification: SpecificationMultiple): List<Character>{
        specification.run {
            return queryList(apiService.getCharactersByMultiple(status, gender, species))
        }
    }

    private fun queryListByName(value:String): List<Character> {
        return queryList(apiService.getCharactersByName(value))
    }

    private fun queryListByStatus(value:String): List<Character> {
        return queryList(apiService.getCharactersByStatus(value))
    }

    private fun queryListBySpecies(value:String): List<Character> {
        return queryList(apiService.getCharactersBySpecies(value))
    }

    private fun queryListByGender(value:String): List<Character> {
        return queryList(apiService.getCharactersByGender(value))
    }

    override fun queryItem(specification: Specification): Character {
        return when (specification) {
            is SpecificationById -> queryItem(apiService.getCharacterById(specification.id))
            else -> getAll()[0]
        }
    }
}