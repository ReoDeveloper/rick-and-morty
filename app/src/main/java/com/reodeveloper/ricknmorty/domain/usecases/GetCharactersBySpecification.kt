package com.reodeveloper.ricknmorty.domain.usecases

import android.os.Handler
import android.os.Looper
import com.reodeveloper.ricknmorty.common.Repository
import com.reodeveloper.ricknmorty.common.usecase.Result
import com.reodeveloper.ricknmorty.common.usecase.ResultList
import com.reodeveloper.ricknmorty.common.usecase.UseCase
import com.reodeveloper.ricknmorty.data.Specification
import com.reodeveloper.ricknmorty.domain.model.Character

class GetCharactersBySpecification(repository: Repository<Character>, private val specification: Specification) : UseCase<Character>(repository) {
    override fun execute(callback: Result<Character>){
        val result = repository.queryList(specification)
        val runnable = Runnable {
            if(callback is ResultList<Character>){
                callback.success(result)
            }else{
                callback.error("Wrong callback. Result must be returned as a List")
            }
        }
        Handler(Looper.getMainLooper()).post(runnable)
    }
}