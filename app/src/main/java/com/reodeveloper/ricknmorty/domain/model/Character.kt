package com.reodeveloper.ricknmorty.domain.model

data class Character(val id: Int, val name: String, val gender: String, val created: String, val imageUrl: String,
                     val status: String, val species: String, val type: String, val origin: String, val location: String)