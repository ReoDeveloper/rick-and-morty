package com.reodeveloper.ricknmorty.domain.usecases

import android.os.Handler
import android.os.Looper
import com.reodeveloper.ricknmorty.common.Repository
import com.reodeveloper.ricknmorty.data.Specification
import com.reodeveloper.ricknmorty.common.usecase.Result
import com.reodeveloper.ricknmorty.common.usecase.ResultItem
import com.reodeveloper.ricknmorty.common.usecase.UseCase
import com.reodeveloper.ricknmorty.domain.model.Character

class GetCharacterById(repository: Repository<Character>, private val specification: Specification) : UseCase<Character>(repository) {
    override fun execute(callback: Result<Character>) {
        val result = repository.queryItem(specification)
        val runnable = Runnable {
            if(callback is ResultItem<Character>){
                callback.success(result)
            }else{
                callback.error("Wrong callback. Result must be returned as an item")
            }
        }
        Handler(Looper.getMainLooper()).post(runnable)
    }
}