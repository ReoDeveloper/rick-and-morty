package com.reodeveloper.ricknmorty.common

import android.content.Context
import com.reodeveloper.ricknmorty.data.*
import com.reodeveloper.ricknmorty.data.retrofit.ApiCharacter
import com.reodeveloper.ricknmorty.data.retrofit.RetrofitDataSource
import com.reodeveloper.ricknmorty.data.retrofit.RfCharacterMapper
import com.reodeveloper.ricknmorty.data.room.DbCharacter
import com.reodeveloper.ricknmorty.data.room.DbCharacterMapper
import com.reodeveloper.ricknmorty.data.room.RoomDataSource
import com.reodeveloper.ricknmorty.domain.model.Character
import com.reodeveloper.ricknmorty.domain.usecases.GetAllCharacters
import com.reodeveloper.ricknmorty.domain.usecases.GetCharacterById
import com.reodeveloper.ricknmorty.domain.usecases.GetCharactersBySpecification

class UseCaseProvider {
    companion object {
        fun provideGetAllCharactersUseCase(context:Context):GetAllCharacters =
                GetAllCharacters(RepositoryProvider.provideCharacterRepository(context))
        fun provideGetCharacterById(context:Context, idCharacter:Int): GetCharacterById =
                GetCharacterById(RepositoryProvider.provideCharacterRepository(context), SpecificationById(idCharacter))
        fun provideGetCharactersBySpecification(context:Context, specification:Specification):GetCharactersBySpecification =
                GetCharactersBySpecification(RepositoryProvider.provideCharacterRepository(context), specification)
    }
}

class RepositoryProvider {
    companion object {
        fun provideCharacterRepository(context:Context):Repository<Character> =
                CharactersRepository(DataSourceProvider.provideCharacterApiDataSource())
                        .apply { this.cache = DataSourceProvider.provideCharacterDbDataSource(context) }
    }
}

class DataSourceProvider {
    companion object {
        fun provideCharacterApiDataSource(): DataSource<Character> =
                RetrofitDataSource(MapperProvider.provideApiCharacterMapper())
        fun provideCharacterDbDataSource(context: Context): DataSource<Character> =
                RoomDataSource(context, MapperProvider.provideDbCharacterMapper())
    }
}

class MapperProvider {
    companion object {
        fun provideApiCharacterMapper(): Mapper<ApiCharacter, Character> = RfCharacterMapper()
        fun provideDbCharacterMapper(): TwoWayMapper<DbCharacter, Character> = DbCharacterMapper()
    }
}