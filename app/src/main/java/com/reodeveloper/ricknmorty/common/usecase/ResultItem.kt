package com.reodeveloper.ricknmorty.common.usecase

interface ResultItem<T> : Result<T> {
    fun success(item: T)
}