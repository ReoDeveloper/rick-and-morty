package com.reodeveloper.ricknmorty.common.usecase

interface ResultList<T> : Result<T> {
    fun success(items: List<T>)
}