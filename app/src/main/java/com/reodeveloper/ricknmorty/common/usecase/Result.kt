package com.reodeveloper.ricknmorty.common.usecase

interface Result<T> {
    fun error(message:String)
}