package com.reodeveloper.ricknmorty.common.usecase

import com.reodeveloper.ricknmorty.common.Repository

abstract class UseCase<T>(val repository: Repository<T>) {
    abstract fun execute(callback: Result<T>)
}
