package com.reodeveloper.ricknmorty.data.retrofit

import org.junit.Assert.assertEquals
import org.junit.Test

class RfCharacterMapperTest {

    private val mapper = RfCharacterMapper()

    companion object {
        val EMPTY = ""
        val DUMMY_NUMBER = 1
        val DUMMY_STRING = "Something valid"
        val DUMMY_ORIGIN = ApiOrigin(DUMMY_STRING, DUMMY_STRING)
        val EMPTY_ORIGIN = ApiOrigin(EMPTY, EMPTY)
        val DUMMY_LOCATION = ApiLocation(DUMMY_STRING, DUMMY_STRING)
        val EMPTY_LOCATION = ApiLocation(EMPTY, EMPTY)
        val DUMMY_LIST:List<String> = emptyList()
        val EMPTY_LIST:List<String> = emptyList()
        val NULL = null
    }

    @Test fun shouldReturnValidCharacter() {
        val given = givenApiCharacter()

        val test = mapper.map(given)

        assertEquals(DUMMY_NUMBER, test.id)
        assertEquals(DUMMY_STRING, test.name)
        assertEquals(DUMMY_STRING, test.status)
        assertEquals(DUMMY_STRING, test.species)
        assertEquals(DUMMY_STRING, test.type)
        assertEquals(DUMMY_STRING, test.gender)
        assertEquals(DUMMY_STRING, test.origin)
        assertEquals(DUMMY_STRING, test.location)
        assertEquals(DUMMY_STRING, test.imageUrl)
        assertEquals(DUMMY_STRING, test.created)
    }

    @Test fun shouldReturnCharacterName() {
        val given = givenApiCharacterWithName()

        val test = mapper.map(given)

        assertEquals(DUMMY_STRING, test.name)
    }

    @Test fun shouldReturnCharacterStatus() {
        val given = givenApiCharacterWithStatus()

        val test = mapper.map(given)

        assertEquals(DUMMY_STRING, test.status)
    }

    @Test fun shouldReturnCharacterSpecies() {
        val given = givenApiCharacterWithSpecies()

        val test = mapper.map(given)

        assertEquals(DUMMY_STRING, test.species)
    }

    @Test fun shouldReturnCharacterType() {
        val given = givenApiCharacterWithType()

        val test = mapper.map(given)

        assertEquals(DUMMY_STRING, test.type)
    }

    @Test fun shouldReturnCharacterGender() {
        val given = givenApiCharacterWithGender()

        val test = mapper.map(given)

        assertEquals(DUMMY_STRING, test.gender)
    }

    @Test fun shouldReturnCharacterOrigin() {
        val given = givenApiCharacterWithOrigin()

        val test = mapper.map(given)

        assertEquals(DUMMY_STRING, test.origin)
    }

    @Test fun shouldReturnCharacterLocation() {
        val given = givenApiCharacterWithLocation()

        val test = mapper.map(given)

        assertEquals(DUMMY_STRING, test.location)
    }

    @Test fun shouldReturnCharacterImageUrl() {
        val given = givenApiCharacterWithAvatar()

        val test = mapper.map(given)

        assertEquals(DUMMY_STRING, test.imageUrl)
    }

    @Test fun shouldReturnCharacterCreatedTime() {
        val given = givenApiCharacterWithCreatedTime()

        val test = mapper.map(given)

        assertEquals(DUMMY_STRING, test.created)
    }

    private fun givenApiCharacter() = ApiCharacter(DUMMY_NUMBER, DUMMY_STRING, DUMMY_STRING,
            DUMMY_STRING, DUMMY_STRING, DUMMY_STRING, DUMMY_ORIGIN, DUMMY_LOCATION, DUMMY_STRING,
            DUMMY_LIST, DUMMY_STRING, DUMMY_STRING)

    private fun givenApiCharacterWithName() = ApiCharacter(DUMMY_NUMBER, DUMMY_STRING, EMPTY,
            EMPTY, EMPTY, EMPTY, EMPTY_ORIGIN, EMPTY_LOCATION, EMPTY,
            EMPTY_LIST, EMPTY, EMPTY)

    private fun givenApiCharacterWithStatus() = ApiCharacter(DUMMY_NUMBER, EMPTY, DUMMY_STRING,
            EMPTY, EMPTY, EMPTY, EMPTY_ORIGIN, EMPTY_LOCATION, EMPTY,
            EMPTY_LIST, EMPTY, EMPTY)

    private fun givenApiCharacterWithSpecies() = ApiCharacter(DUMMY_NUMBER, EMPTY, EMPTY,
            DUMMY_STRING, EMPTY, EMPTY, EMPTY_ORIGIN, EMPTY_LOCATION, EMPTY,
            EMPTY_LIST, EMPTY, EMPTY)

    private fun givenApiCharacterWithType() = ApiCharacter(DUMMY_NUMBER, EMPTY, EMPTY,
            EMPTY, DUMMY_STRING, EMPTY, EMPTY_ORIGIN, EMPTY_LOCATION, EMPTY,
            EMPTY_LIST, EMPTY, EMPTY)

    private fun givenApiCharacterWithGender() = ApiCharacter(DUMMY_NUMBER, EMPTY, EMPTY,
            EMPTY, EMPTY, DUMMY_STRING, EMPTY_ORIGIN, EMPTY_LOCATION, EMPTY,
            EMPTY_LIST, EMPTY, EMPTY)

    private fun givenApiCharacterWithOrigin() = ApiCharacter(DUMMY_NUMBER, EMPTY, EMPTY,
            EMPTY, EMPTY, EMPTY, DUMMY_ORIGIN, EMPTY_LOCATION, EMPTY,
            EMPTY_LIST, EMPTY, EMPTY)

    private fun givenApiCharacterWithLocation() = ApiCharacter(DUMMY_NUMBER, EMPTY, EMPTY,
            EMPTY, EMPTY, EMPTY, EMPTY_ORIGIN, DUMMY_LOCATION, EMPTY,
            EMPTY_LIST, EMPTY, EMPTY)

    private fun givenApiCharacterWithAvatar() = ApiCharacter(DUMMY_NUMBER, EMPTY, EMPTY,
            EMPTY, EMPTY, EMPTY, EMPTY_ORIGIN, EMPTY_LOCATION, DUMMY_STRING,
            EMPTY_LIST, EMPTY, EMPTY)

    private fun givenApiCharacterWithCreatedTime() = ApiCharacter(DUMMY_NUMBER, EMPTY, EMPTY,
            EMPTY, EMPTY, EMPTY, EMPTY_ORIGIN, EMPTY_LOCATION, EMPTY,
            EMPTY_LIST, EMPTY, DUMMY_STRING)

}