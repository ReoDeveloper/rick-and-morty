package com.reodeveloper.ricknmorty

import com.reodeveloper.ricknmorty.common.MapperProvider
import com.reodeveloper.ricknmorty.data.retrofit.RetrofitDataSource
import com.reodeveloper.ricknmorty.domain.model.Character
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertTrue
import org.junit.Before
import org.junit.Test

class ApiClientTest: MockWebServerTest() {

    private lateinit var apiClient: RetrofitDataSource

    @Before
    override fun setUp() {
        super.setUp()
        val mockWebServerEndpoint = baseEndpoint
        apiClient = RetrofitDataSource(MapperProvider.provideApiCharacterMapper(), mockWebServerEndpoint)
    }

    @Test fun sendsGetCharactersToTheCorrectEndpoint(){
        enqueueMockResponse(200, "getFirstPage.json")

        apiClient.getAll()

        assertGetRequestSentTo("/character?page=1")
    }

    @Test fun parsesCharactersProperlyGettingAllTheCharacters(){
        enqueueMockResponse(200, "getFirstPage.json")

        val result = apiClient.getAll()

        assertEquals(20, result.size.toLong())
        assertCharacterContainsExpectedValues(result[0])
    }

    @Test(expected= UnsupportedOperationException::class)
    fun throwsUnsupportedOperationExceptionOnStoreItem(){
        enqueueMockResponse(200)

        apiClient.store(givenAnyCharacter())
    }

    @Test(expected= UnsupportedOperationException::class)
    fun throwsUnsupportedOperationExceptionOnStoreList(){
        enqueueMockResponse(200)

        apiClient.store(givenListOfAnyCharacters())
    }

    private fun assertCharacterContainsExpectedValues(item:Character?){
        assertTrue(item != null)
        assertEquals(1, item?.id)
        assertEquals("Rick Sanchez", item?.name)
        assertEquals("Alive", item?.status)
        assertEquals("Human", item?.species)
        assertEquals("", item?.type)
        assertEquals("Male", item?.gender)
        assertEquals("Earth (C-137)", item?.origin)
        assertEquals("Earth (Replacement Dimension)", item?.location)
        assertEquals("https://rickandmortyapi.com/api/character/avatar/1.jpeg", item?.imageUrl)
        assertEquals("Sat, 4 Nov 2017 19:48:46", item?.created)
    }

    private fun givenListOfAnyCharacters() : List<Character> {
        return listOf(givenAnyCharacter(), givenAnyCharacter(), givenAnyCharacter())
    }

    private fun givenAnyCharacter():Character {
        return Character(
                1,
                "Rick Sanchez",
                "Male",
                "Sat, 4 Nov 2017 19:48:46",
                "https://rickandmortyapi.com/api/character/avatar/1.jpeg",
                "Alive",
                "Human",
                "",
                "Earth (C-137)",
                "Earth (Replacement Dimension)")
    }
}