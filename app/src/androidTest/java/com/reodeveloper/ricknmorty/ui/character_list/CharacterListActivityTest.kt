package com.reodeveloper.ricknmorty.ui.character_list

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.intent.Intents
import android.support.test.espresso.intent.Intents.intended
import android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import com.reodeveloper.ricknmorty.CustomMatcher.Companion.withTitle
import com.reodeveloper.ricknmorty.R
import com.reodeveloper.ricknmorty.ui.character_item.CharacterActivity
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class CharacterListActivityTest {

    companion object {
        val MORTY = 1
        val MORTY_NAME = "Morty Smith"
    }

    @get:Rule
    public var mActivityRule : ActivityTestRule<CharacterListActivity> = ActivityTestRule(CharacterListActivity::class.java, true, true)

    @Before
    fun setupIntents(){
        Intents.init()
    }

    @After
    fun releaseIntents(){
        Intents.release()
    }

    @Test fun clickListItem_openDetailActivity(){
        onView(ViewMatchers.withId(R.id.recyclerView))
                .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(MORTY, click()))

        intended(hasComponent(CharacterActivity::class.java.name))
        onView(withId(R.id.collapsing_toolbar_layout)).check(matches(withTitle(MORTY_NAME)))
    }

}