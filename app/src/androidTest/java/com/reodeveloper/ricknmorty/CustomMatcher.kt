package com.reodeveloper.ricknmorty

import android.support.design.widget.CollapsingToolbarLayout
import android.support.test.espresso.matcher.BoundedMatcher
import android.view.View
import org.hamcrest.Description
import org.hamcrest.Matcher

class CustomMatcher {
    companion object {
        fun withTitle(title: String): Matcher<View> {
            return object : BoundedMatcher<View, CollapsingToolbarLayout>(CollapsingToolbarLayout::class.java) {
                override fun describeTo(description: Description?) {
                    description?.appendText("CollapsingToolbarLayout with title: $title")
                }

                override fun matchesSafely(item: CollapsingToolbarLayout?): Boolean {
                    return item?.title == title
                }
            }
        }
    }
}